# rdex-pi-crawler

A simple Node script to test Rdex api.

## Requirements

- Nodejs > 10

## Install

simple as `npm install`

## Start

eg: `node main.js -a http://api.test.ouestgo.fr -k rdex_mobicoop -p "acompletelyfakeprivateKey"`