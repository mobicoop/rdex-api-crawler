'use strict';

const request = require('request'),
  crypto = require('crypto'),
  kuler = require('kuler'),
  fs = require('fs'),
  jsonFormat = require('json-format'),
  program = require('commander');

program
  .version('1.0.0')
  .option('-a, --api Url api to contact <link>')
  .option('-k, --key-name Api key to use <string>')
  .option('-p, --private-key Private Key to use <string>')
  .parse(process.argv);

// All the tree args are needed
if(!program.api || !program.keyName || !program.privateKey){
  console.error(kuler('All the params are needed !','red'));
  program.help();
  process.exit(1);
}

// Create the Hmac with name key
let hmac = crypto.createHmac('sha256', program.privateKey);

// Get current timestamp & create url to search Nancy-Metz
let timeStamp = Date.now(),
  apiUrl = `${program.api}/restapi/journeys.json?timestamp=${timeStamp}&apikey=${program.keyName}&p[driver][state]=1&p[passenger][state]=1&p[from][latitude]=48.69278&p[from][longitude]=6.18361&p[to][latitude]=49.11972&p[to][longitude]=6.17694`;


// Crypt the url
hmac.update(apiUrl);
// Get the string crypted
let signature = hmac.digest('hex'),
  apiUrlSigned = `${apiUrl}&signature=${signature}`;


// We add some options to request api
let options = {
  url: apiUrlSigned,
  headers: {
    'User-Agent': 'Mozilla/5.0 (platform; rv:geckoversion) Gecko/geckotrail Firefox/firefoxversion'
  }
};

// Request the api Now 🕰
request(apiUrlSigned, (error, response, body) =>{
  // if there is an error we stopped here
  if(error) return console.error(kuler(error, 'red'));
  if(response.statusCode !== 200) console.warn(`Not a good answer, status is ${response.statusCode}`,'orange');
   // config format Json
  let config = {
    type: 'space',
    size: 2
  }
  let prettyBody = jsonFormat(JSON.parse(body), config);
  fs.writeFileSync('out.json', prettyBody);
  console.log(typeof body);
  console.log(kuler('Data has been written to out.json','green'));
});